
a = {10, 20, 30, 40, 80}
b = {100, 30, 80, 40, 60}
a.difference_update(b)
print(a)


auto = {"car", "bus", "moto"}
auto.discard("bus")
print(auto)

statusProd = {"update", "check", "tested on a test"}
statusTest = {"test", "check", "update"}
statusProd.intersection_update(statusTest)
print(statusProd)


x = {"car", "bus", "moto"}
y = {"plane", "helicopter", "air balloon"}
fin = x.isdisjoint(y)
print(fin)


x = {"a", "b", "c"}
y = {"f", "e", "d", "c", "b", "a"}
z = x.issubset(y)
print(z)


t = {"car", "bus", "moto"}
v = {"plane", "moto", "car"}
w = t.symmetric_difference(v)
print(w)


s = {"car", "bus", "moto"}
c = {"car", "helicopter", "air balloon"}
s.symmetric_difference_update(c)
print(s)

u = {"car", "bus", "moto"}
p = {"car", "helicopter", "air balloon"}
k = u.union(p)
print(k)