from pizzastore.objects.pizza import Pizza


class ReceiptLine:

    def __init__(self, pizza: Pizza, num: int):
        self.pizza = pizza
        self.num = num

    def __str__(self):
        return f'{self.pizza.name:<20} * {self.num} = {self.pizza.price * self.num}\n'