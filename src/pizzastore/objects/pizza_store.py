from pizzastore.objects.pizza import Pizza
import csv
import os


class SingletonMeta(type):
    """
    В Python класс Одиночка можно реализовать по-разному. Возможные способы
    включают себя базовый класс, декоратор, метакласс. Мы воспользуемся
    метаклассом, поскольку он лучше всего подходит для этой цели.
    """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        Данная реализация не учитывает возможное изменение передаваемых
        аргументов в `__init__`.
        """
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class PizzaStore(metaclass=SingletonMeta):

    def __init__(self):
        self.pizzas = []
        self.read_pizzas('pizzastore/helpers/info.csv')
        self.receipts = []

    def print_receipts(self):
        for item in self.receipts:
            print(item)

    def add_receipts(self, receipt):
        self.receipts.append(receipt)

    def add_pizza(self, pizza):
        self.pizzas.append(pizza)

    def print_menu(self):
        for item in self.pizzas:
            print(item)

    def read_pizzas(self, file: str):
        with open(file, 'rt', encoding='utf-8') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                self.pizzas.append(
                    Pizza(row['idx'], row['name'], row['price'], row['description'])
                )

    def write_pizzas(self, file: str):
        with open(file, 'wt', encoding='utf-8') as csvfile:
            fieldnames = ['idx', 'name', 'description', 'price']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for item in self.pizzas:
                writer.writerow({
                    'idx': item.idx,
                    'name': item.name,
                    'price': item.price,
                    'description': item.description
                })

    def new_pizza(self, idx, name, price, description):
        self.pizzas.append(Pizza(idx, name, price, description))
