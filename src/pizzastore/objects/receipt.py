import datetime
from pizzastore.objects.receiptline import ReceiptLine

class Receipt:
    n_receipt = 1

    def __init__(self):
        self.date = datetime.date.today()
        self.time = datetime.datetime.now()
        self.lines = []
        self.number = Receipt.n_receipt
        Receipt.n_receipt += 1

    def add_line(self, line: ReceiptLine) -> object:
        self.lines.append(line)

    def __str__(self):
        time = str(self.time)
        rornumber = str(self.number)
        return f'Number of receipt: {rornumber}\n{time}\n{"".join(str(item) for item in self.lines)}'

