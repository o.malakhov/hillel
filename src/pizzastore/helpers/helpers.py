def low_price(lst, value):
    return [item for item in lst if item.price < value]


def sort_first_item(lst, value):
    return [item for item in lst if item.name.startswith(value)]


def decorator(decor_func):
    def inner(lst):
        print('*' * 140)
        decor_func(lst)
        print('~' * 140)
        print('Total:', len(lst))
    return inner

@decorator
def print_list(lst):
    for item in lst:
        print(item)
