from pizzastore.objects.pizza_store import PizzaStore
from constants.pizzas_info import pizzas
from helpers.helpers import print_list, low_price, sort_first_item
from pizzastore.objects.receipt import Receipt
from pizzastore.objects.receiptline import ReceiptLine



def run_pizzastore():
    while True:
        print('«Pizza Hut» - це замовлення і доставка піци для будь-якої компанії і на будь-який смак! Замовляйте піцу,'
              ' і ми подбаємо про те, щоб Ви насолодилися смаком справжньої піци, не докладаючи при цьому ніяких зусиль!')
        ch = input('? ')
        match ch:
            case '0':
                break
            case '1':
                ps.print_menu()
            case '2':
                 try:
                     x = int(input("Введите сумму "))
                     if x < 0:
                         raise Exception("Введите корректную сумму")
                 except Exception as e:
                     print(e)
                 low_price_pizzas = low_price(pizzas, x)
                 print_list(low_price_pizzas)
            case '3':
                 vegan_pizza = sort_first_item(pizzas, 'Vegan')
                 print_list(vegan_pizza)
            case '4':
                receipt = Receipt()
                receipt.add_line(ReceiptLine(ps.pizzas[0], 1))
                receipt.add_line(ReceiptLine(ps.pizzas[3], 2))
                print(receipt)
            case '5':
                ps.write_pizzas('pizzastore/helpers/info1.csv')
            case '6':
                ps.new_pizza(12,'Pizza Jamaica Bombastic', 160, 'Chicken, sauce')
            case _:
                print('Неизвестная команда')



if __name__ == '__main__':
    ps = PizzaStore()
    print('->', ps.__class__)
    run_pizzastore()