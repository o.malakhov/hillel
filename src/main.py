# a) 2 * 3
a = 2
b = 3
c = a * b
print(c, type(c))

# b) (3 * 3 + 8) / 3
a2 = 3
b2 = 8
c2 = (3 * 3 + 8) / 3
print (c2, type(c2))

# c) 8 // 3
a3 = 8
b3 = 3
c3 = a3 // b3
print(c3, type(c3))

# d) 8 % 3
a4 = 8
b4 = 3
c4 = a4 % b4
print(c4, type(c4))

# e) 5 ** 2
a5 = 5
b5 = 2
c5 = a5 ** b5
print(c5, type(c5))

# f) ‘Hello ’ + ‘world’
a6 = 'Hello '
b6 = 'world'
c6 = a6 + b6
print(c6, type(c6))

# Task 2
var1 = 'Imagination rules the world'
var2 = "Success doesn't come to you…you go to it"
print(var2)

# Task 3
# a) вывести первые 10 символов строки
print(var1 [:10])

# b) вывести 10 символов, начиная с 3-го
print(var2 [2:12])

# c) вывести последние 10 символов строки
print(var1 [-10:])

# d) вывести всю строку в обратном порядке
print(var1 [::-1])

# e) вывести буквы с нечетным индексом и четным
print(var1 [::2])
print(var1 [1::2])

# 4. Реализовать интерактивный калькулятор для сложения двух чисел.
# Спросить у пользователя первое число, второе число и вывести результат сложения (использовать преобразование типов)

num1 = input("Введите первое число и нажмите Enter ")
num2 = input("Введите второе число и нажмите Enter ")
result = float(num1) + float(num2)
print("Сумма ваших числе =", result)