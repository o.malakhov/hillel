# Реализовать программу для нахождения корней квадратного уравнения
import math

print('Введите коэффиценты для уравнения')
a = float(input('a = '))
b = float(input('b = '))
c = float(input('c = '))

discr = b ** 2 - 4 * a * c
print('Дискрименант D =', discr)

if discr != 0:
    x1 = (-b + ((discr)**(1/2))) / (2 * a)
    x2 = (-b - ((discr)**(1/2))) / (2 * a)
    if discr > 0:
        print('Вещественные корни', 'x1 = ', x1, 'x2 = ', x2)
    else:
        print('Действительных корней нет', 'x1 = ', x1, 'x2 = ', x2)
else:
    x = -b / (2 * a)
    print('x =', x)